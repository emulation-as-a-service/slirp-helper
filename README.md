Example:

```shell
LD_LIBRARY_PATH=. ./vde_plug /tmp/switch -- ./slirp-helper --fd 3

# or
LD_LIBRARY_PATH=. ./vde-spawn /tmp/switch ./slirp-helper --fd 3

# No internal DCHP server:
LD_LIBRARY_PATH=. ./vde_plug /tmp/switch -- ./slirp-helper --fd 3 --dhcp-start 0.0.0.0
```
