from ubuntu:18.04

run apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
  meson libdbus-1-dev \
  build-essential \
	libglib2.0-dev \
	autoconf \
	libtool \
	curl libdbus-1-dev \
  libvdeplug-dev python3 python3-pip
run pip3 install meson

run curl https://sh.rustup.rs -sSf -o /rustup.sh
run chmod +x /rustup.sh
run /rustup.sh -y

workdir /src
copy . .
run printf 'git-commit: %s\0' \
  $(git describe --dirty --all --abbrev=128 --long --always) > .git-commit
run cd vde-spawn && make && cp -P vde-spawn /opt
run meson build libslirp && cd build && ninja install \
  && rm -rf *.p && cp -P *.so* /opt
run cd libslirp-rs && /root/.cargo/bin/cargo build --all-features --release \
  && cp target/release/libslirp-helper /opt
run cd vde-2 && autoreconf --install && ./configure && make install && \
  cp -P /usr/local/bin/vde_plug /usr/local/lib/libvdeplug.so* /opt
workdir /opt
run for i in *; do \
  objcopy --add-section .note.git-commit=/src/.git-commit "$i"; done || true
copy slirp-helper .
