#include <assert.h>
#include <libvdeplug.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
  assert(dup2(1, 3) == 3);
  assert(dup2(vde_datafd(vde_open(argv[1], argv[2], 0)), 3) == 3);
  assert(!execvp(argv[2], &argv[2]));
}
